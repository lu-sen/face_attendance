#include <cstdio>
#include <cstdlib> //malloc
#include <cstring> //memset
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h> //sockaddr_in
#include <arpa/inet.h> //inet_ntop
#include <signal.h>
#include <pthread.h>
#include <unistd.h> //sleep
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

#define BOUNDARY "video boundary--"
VideoCapture cam;

Mat frame;
pthread_mutex_t frame_mutex = PTHREAD_MUTEX_INITIALIZER;

Rect range;
pthread_mutex_t range_mutex = PTHREAD_MUTEX_INITIALIZER;

int load_file(const char* file, FILE* mfp)
{
    FILE* fp = fopen(file, "r");
    fseek(fp, 0, SEEK_END);
    long size = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    char* buf = (char*)malloc(size);
    fread(buf, 1, size, fp);
    fclose(fp);
    fwrite(buf, 1, size, mfp);
    free(buf);

    return 0;
}

void send_file(const char* file, FILE* sfp)
{
    char* response = NULL;
    size_t resplen = 0;

    FILE* mfp = open_memstream(&response, &resplen);
    fprintf(mfp, "Content-Type: image/jpeg\r\n\r\n");
    load_file(file, mfp);
    fprintf(mfp, "\r\n\r\n--%s\r\n", BOUNDARY);
    fclose(mfp);

    fprintf(sfp, "%lx\r\n", resplen);
    fwrite(response, 1, resplen, sfp);
    fprintf(sfp, "\r\n");
    free(response);
}

void send_buffer(const char* buf, size_t size, FILE* sfp)
{
    char* response = NULL;
    size_t resplen = 0;

    FILE* mfp = open_memstream(&response, &resplen);
    fprintf(mfp, "Content-Type: image/jpeg\r\n\r\n");
    fwrite(buf, 1, size, mfp);
    fprintf(mfp, "\r\n\r\n--%s\r\n", BOUNDARY);
    fclose(mfp);

    fprintf(sfp, "%lx\r\n", resplen);
    fwrite(response, 1, resplen, sfp);
    fprintf(sfp, "\r\n");
    free(response);
}

void* handle_client(void* arg)
{
    char buf[BUFSIZ];

    printf("== thread start\n");

    FILE* sfp = fdopen(*(int*)arg, "r+");
    if (!sfp)
    {
        perror("fdopen");
        return NULL;
    }

    while(fgets(buf, BUFSIZ, sfp) && strlen(buf) > 2)
    {
        printf(buf);
    }

    fprintf(sfp, "HTTP/1.1 200 OK\r\n");
    fprintf(sfp, "Content-Type: multipart/x-mixed-replace; boundary=%s\r\n", BOUNDARY);
    fprintf(sfp, "Transfer-Encoding: chunked\r\n");
    fprintf(sfp, "\r\n");

    Mat image;

    while(1)
    {
        vector<unsigned char> buf;
        pthread_mutex_lock(&frame_mutex);
        image = frame;
        pthread_mutex_unlock(&frame_mutex);

        pthread_mutex_lock(&range_mutex);
        rectangle(image, range, CV_RGB(255, 0, 0));
        pthread_mutex_unlock(&range_mutex);

        imencode(".jpg", image, buf);
        send_buffer((char*)buf.data(), buf.size(), sfp);
        usleep(40); //25fps

        //send_file("black.jpg", sfp);
        //sleep(1);
        //send_file("white.jpg", sfp);
        //sleep(1);
    }

    fclose(sfp);

    printf("== thread exit\n");
    return NULL;
}

void* capture(void*)
{
    puts("camera start");

    while(1)
    {
        cam >> frame;
        usleep(20); //50 fps
    }
}

void* detector(void*)
{
    CascadeClassifier classifier;
    classifier.load("/usr/share/opencv/haarcascades/haarcascade_frontalface_alt2.xml");
    Mat gray;
    vector<Rect> faces;

    puts("face detection start");

    while(1)
    {
        cvtColor(frame, gray, COLOR_BGR2GRAY);
		equalizeHist(gray, gray);
        classifier.detectMultiScale(gray, faces, 2, 1);
        if (faces.size())
        {
            pthread_mutex_lock(&range_mutex);
            range = faces[0];
            pthread_mutex_unlock(&range_mutex);
        }
    }
}

int main()
{
    cam.open(0);
    cam >> frame; //make sure frame is not empty

    pthread_t capture_tid;
    pthread_t detector_tid;
    pthread_create(&capture_tid, NULL, capture, NULL);
    pthread_create(&detector_tid, NULL, detector, NULL);

    signal(SIGPIPE, SIG_IGN);

    int listenfd = socket(AF_INET, SOCK_STREAM, 0);

    if (listenfd < 0)
    {
        perror("socket");
        return 1;
    }

    int opt = 1;
    setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

    struct sockaddr_in saddr;
    memset(&saddr, 0, sizeof(saddr));

    saddr.sin_family = AF_INET;
    saddr.sin_addr.s_addr = htonl(INADDR_ANY);
    saddr.sin_port = htons(8000);
    if (bind(listenfd, (struct sockaddr *)&saddr, sizeof(saddr)) < 0)
    {
        perror("bind");
        return 1;
    }

    if (listen(listenfd, 64) < 0)
    {
        perror("listen");
        return 1;
    }

    puts("web server start");

    while (1)
    {
        struct sockaddr_in caddr;
        socklen_t addrlen = sizeof(caddr);
        int connfd;
        connfd = accept(listenfd, (struct sockaddr *)&caddr, &addrlen);

        char ipstr[INET_ADDRSTRLEN];
        inet_ntop(AF_INET, &caddr.sin_addr, ipstr, sizeof ipstr);
        printf("client %s connected\n", ipstr);
        if (connfd < 0)
        {
            perror("accept");
            continue;
        }

        pthread_t tid;
        pthread_create(&tid, NULL, handle_client, &connfd);
    }
}
