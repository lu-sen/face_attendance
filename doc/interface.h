#ifndef INTERFACE_H
#define INTERFACE_H

#include <opencv2/opencv.hpp>
#include <string>
#include <vector>
#include <time.h>
#include <sqlite3.h>

struct Employee
{
    std::string depart_id; //<部门
    std::string id; //< 工号
    std::string info; //< 员工信息
    double score; //< 相似度得分
};

class Face: public QObject
{
public:
    Face(std::string& model);

    /**
     * 检测图像中是否存在人脸
     * @param image 待检测图像
     * @param face 人脸的范围
     * @retval 1 找到人脸
     * @retval 0 没有找到人脸
     */
    int detect(cv::Mat image, cv::Rect& face);

    /**
     * 识别人脸
     * 创建线程，将人脸图像进行压缩和BASE64编码后发送给百度云
     * 解析百度云API返回的结果：
     * 1. 如果识别成功，调用回调函数，并将结果传给回调函数。
     * 2. 如果识别失败，不调用回调函数。
     * @param face 待识别的人脸图像
     * @retval 1 success
     * @retval 0 fail
     */
    int recognize(cv::Mat face, Employee& e);

private:
    cv::CascadeClassifier classifier;
};

class Database
{
public:
    Database(std::string dbfile);

    /**
     * 添加打卡记录
     * @param id 员工工号
     * @param time 打卡时间，Epoch纪元时间，从1970/1/1到现在的秒数
     * @retval 0 打卡成功
     */
    int punch(std::string id, time_t time);

    /**
     * 根据工号查找员工姓名
     * @param id 员工工号
     * @param name 员工姓名
     * @retval 0 成功
     */
    int getname(std::string id, std::string& name);

    ~Database();
private:
    sqlite3* db;
};

#endif
